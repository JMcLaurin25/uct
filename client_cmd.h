#ifndef CLIENT_CMD_H
#define CLIENT_CMD_H

int login(char *);
ssize_t send_data(int sock_d, char *sendBuf);
void pong(int sock_d);

void handler(int sig_num);
int room_part(char *buffer, char *cur_room, int buff_sz);
int room_clear(char *cur_room, int sock_d);

#endif
