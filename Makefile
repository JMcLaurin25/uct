
CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic -fno-stack-protector
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

uct: uct.o client_cmd.o

.PHONY: clean debug profile

clean:
	-rm *.o *.su uct

debug: CFLAGS+=-g
debug: uct

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: uct

