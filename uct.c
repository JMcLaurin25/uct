
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ncurses.h>

#include <signal.h>

#include "client_cmd.h"

#define MAX_BUFFER 256
#define MAX_LENGTH 16
#define CMD_LENGTH 10

int main(void)
{
	// Signal handle for ctrl-c
	signal(SIGINT, handler);

	struct sockaddr_in miniirc = {0};
	char *recvBuf = calloc(MAX_BUFFER, sizeof(char));
	char *sendBuf = calloc(MAX_BUFFER, sizeof(char));

	memset(recvBuf, '\0', MAX_BUFFER);
	memset(sendBuf, '\0', MAX_BUFFER);

	int sock_d = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_d < 0) {
		free(recvBuf);
		free(sendBuf);
		perror("Socket creation failed");
		return -2;
	}

	// Hardcoded port and address for project criteria
	miniirc.sin_family = AF_INET;
	miniirc.sin_port = htons(6667);
	miniirc.sin_addr.s_addr = inet_addr("127.0.0.1");

	if (connect(sock_d, (struct sockaddr *)&miniirc, sizeof(miniirc)) < 0) {
		perror("Failed to connect to server");
		free(recvBuf);
		free(sendBuf);
		return -3;
	}

	pid_t recvPID;
	recvPID = fork();
	
	if (recvPID < 0 ) {
		perror("Failed to fork()");
		free(recvBuf);
		free(sendBuf);
		return -4;
	} else if (recvPID == 0) { // Receiving PID
		// Message reception loop
		for (;;) {
			ssize_t msg_sz = 0;

			//Receive Message
			while ((msg_sz = recv(sock_d, recvBuf, sizeof(recvBuf) - 1, MSG_DONTWAIT)) > 0 ) {
				recvBuf[msg_sz] = '\0';
				printf("%s", recvBuf);
			}
			if (strcasestr(recvBuf, "ping") != NULL) {
				pong(sock_d);
			}

			memset(recvBuf, '\0', MAX_BUFFER);
		}
	} else { // Sending PID (Parent)
		sleep(1);
		ssize_t snd_sz = 0;

		// Automate user login 
		char *login_msg = calloc(MAX_BUFFER, sizeof(char));

		if (login(login_msg) < 0) {
			perror("Error with login user data");
			goto END;
		}

		snd_sz = send_data(sock_d, login_msg);
		if (snd_sz != (ssize_t)strlen(login_msg)) {
			perror("Message transmission error");
		}

		char *cur_room = calloc(MAX_LENGTH, sizeof(char));
		char *temp_room = calloc(MAX_LENGTH, sizeof(char));

		// Data sending loop
		for (;;) {
			snd_sz = 0;

			//Get message from user
			if (fgets(sendBuf, MAX_BUFFER - CMD_LENGTH, stdin) == NULL) {
				perror("Input buffer error");
				continue;
			}
			
			// MSG command
			if (strcasestr(sendBuf, "/msg ") != NULL) {
				memmove(sendBuf, sendBuf + 1, MAX_BUFFER - 1);
				char *temp_msg = calloc(MAX_BUFFER, sizeof(char));
				strncat(temp_msg, "/priv", strlen("/priv"));
				strncat(temp_msg, sendBuf, strlen(sendBuf));

				strncpy(sendBuf, temp_msg, strlen(temp_msg));

				free(temp_msg);
			}


			if (sendBuf[0] == '/') {
				memmove(sendBuf, sendBuf + 1, MAX_BUFFER - 1);
			} else if (strncmp(cur_room, "", sizeof(""))) {
				char *temp_msg = calloc(MAX_BUFFER, sizeof(char));

				strncat(temp_msg, "privmsg #", strlen("privmsg #"));
				strncat(temp_msg, cur_room, strlen(cur_room));
				strncat(temp_msg, " :", strlen(" :"));
				strncat(temp_msg, sendBuf, strlen(sendBuf));
				strncat(temp_msg, "\n", strlen("\n"));

				strncpy(sendBuf, temp_msg, strlen(temp_msg));
				free(temp_msg);
			} 

			// Join a room/leave existing room
			if (strcasestr(sendBuf, "join #") != NULL) {
				if (strncmp(cur_room, "", sizeof("")) != 0) {
					// Part string
					if (room_clear(cur_room, sock_d) != 0) {
						fprintf(stderr, "Error leaving previous room\n");
					}
				}
				sscanf(sendBuf, "join #%s\n", cur_room);
			}
				
			// part a room
			room_part(sendBuf, cur_room, MAX_LENGTH);
		
			// Check for quit
			if (strncmp(sendBuf, "quit\n", strlen(sendBuf)) == 0) {
				printf("Goodbye!\n");
				if (kill(recvPID, SIGKILL) == 0) {
					printf("Child process terminated\n");
				}
				break;
			}

			// Send message
			snd_sz = send_data(sock_d, sendBuf);
			if (snd_sz != (ssize_t)strlen(sendBuf)) {
				perror("Message transmission error");
			}

			memset(sendBuf, '\0', MAX_BUFFER);
		}
		free(temp_room);
		free(cur_room);

END:
		free(login_msg);
	}

	close(sock_d);
	free(recvBuf);
	free(sendBuf);
}
