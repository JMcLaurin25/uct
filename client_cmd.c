#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <signal.h>

#include "client_cmd.h"

#define MAX_BUFFER 256
#define MAX_LENGTH 16

/* Buffer flushing macro discovered at daniweb.com

https://www.daniweb.com/programming/software-development/code/217396/how-to-properly-flush-the-input-stream-stdin-
*/

#define FLUSH_STDIN(x) {if(x[strlen(x)-1]!='\n'){do fgets(flush_buf,16,stdin);while(flush_buf[strlen(flush_buf)-1]!='\n');}else x[strlen(x)-1]='\0';} // MACRO for flushing buffer
char flush_buf[MAX_LENGTH];

static int get_value(char *target)
{
	if (!target) {
		return -1;	
	}
	
	if (fgets(target, MAX_LENGTH, stdin) == NULL) {
		return -2;
	}

	FLUSH_STDIN(target);

	char default_val[] = "None";

	if (strncmp(target, "", sizeof("")) == 0) {
		strncat(target, default_val, strlen(default_val));
	}

	return 0;
}

/*
 * The login() automates the users login credentials and returns a string with
 * the correct formatting for logging in at connection.
 */
int login(char *login)
{
	int ret_val = 0;
	char *username = calloc(MAX_LENGTH, sizeof(char));
	char *realname = calloc(MAX_LENGTH, sizeof(char));
	char *nickname = calloc(MAX_LENGTH, sizeof(char));
	char blank[3] = "0 ";

	printf("Username: ");
	ret_val = get_value(username);
	if (ret_val < 0) {
		fprintf(stderr, "User input error");
		goto ERROR_RET;
	}
	username[strlen(username)] = '\0';

	printf("Realname: ");
	ret_val = get_value(realname);
	if (ret_val < 0) {
		fprintf(stderr, "User input error");
		goto ERROR_RET;
	}
	realname[strlen(realname)] = '\0';

	printf("Nick name: ");
	ret_val = get_value(nickname);
	if (ret_val < 0) {
		fprintf(stderr, "User input error");
		goto ERROR_RET;
	}
	nickname[strlen(nickname)] = '\0';

	// Creation login string
	strncat(login, "USER ", strlen("USER "));
	strncat(login, username, strlen(username));
	strncat(login, " ", strlen(" "));
	strncat(login, blank, strlen(blank));
	strncat(login, blank, strlen(blank));
	strncat(login, realname, strlen(realname));
	strncat(login, "\nNICK ", strlen("\nNICK "));
	strncat(login, nickname, strlen(nickname));

ERROR_RET:

	printf("Your credentials are:\n\tUsername: %s\n\tRealname: %s\n\tNickname: %s",
			username, realname, nickname);

	free(username);
	free(realname);
	free(nickname);

	return (ret_val);
}

/*
 * Sends the character buffer to the targetted socket descriptor
 */
ssize_t send_data(int sock_d, char *sendBuf)
{
	if (!sendBuf) {
		return -1;
	}
	return send(sock_d, sendBuf, strlen(sendBuf), MSG_DONTWAIT);
}

/*
 * Calls the send_data() to reply with a "pong" string
 */
void pong(int sock_d)
{
	ssize_t msg_sz = 0;
	char pong[] = "PONG";

	if ((msg_sz = send_data(sock_d, pong)) != (ssize_t)strlen(pong)) {
		perror("Message transmission error");
	}
}

void handler(int sig_num)
{
	/* This function was created to absorb a CTRL-C or SIGINT and allow
	 * the program to continue.
	 */

	(void) sig_num;
}

/*
 * Handling of the part command
 */
int room_part(char *buffer, char *cur_room, int buff_sz)
{
	if (!buffer) {
		return 1;
	}

	char *temp_room = calloc(buff_sz, sizeof(char));
	if (!temp_room) {
		perror("Error allocation char *");
		return 1;
	}

	if (strcasestr(buffer, "part #") != NULL) {
		sscanf(buffer, "part #%s\n", temp_room);
		// Clear cur_room if leaving it
		if (strncmp(cur_room, temp_room, strlen(temp_room)) == 0) {
			memset(cur_room, 0, buff_sz);
		}
	}

	free(temp_room);
	return 0;
}

int room_clear(char *cur_room, int sock_d) 
{
	char *temp = calloc(MAX_BUFFER, sizeof(char));
	if (!temp) {
		perror("Error allocation char *");
		return 1;
	}

	strncat(temp, "PART #", strlen("PART #"));
	strncat(temp, cur_room, strlen(cur_room));
	strncat(temp, "\n", strlen("\n"));

	send_data(sock_d, temp);

	free(temp);
	return 0;
}
